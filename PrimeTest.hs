{-# LANGUAGE TemplateHaskell #-}
import Test.QuickCheck
import Test.QuickCheck.All
import Test.HUnit
import Prime
import PrimeLib
import qualified Data.Numbers.Primes as P

--lib

prop_intSqrtIsSmallerOrEqualTo (Positive a) = (fromIntegral . intSqrt) a <= (sqrt . fromIntegral) a

prop_intLogIsSmallerOrEqualTo (Positive a) = (fromIntegral . intLog) a <= (log . fromIntegral) a

prop_intLogBaseIsSmallerOrEqualTo (Positive a) (Positive x) = (fromIntegral (intLogBase a b)) <= ((log . fromIntegral) a) / ((log . fromIntegral) b)
    where
      b = x+1

prop_theory_JacobiSymbol1 (NonZero a) (NonZero n) = gcd a n == gcd (a `mod` n) n
prop_theory_JacobiSymbol2 (NonZero a) (NonZero n) = (a `mod` n /= 0) ~> (gcd a n == gcd (n `mod` (a `mod` n)) (a `mod` n))

prop_jacobiSymbol_1stRule a (NonNegative n) = -2 < j && j < 2
    where
      n' = (n*2)+1
      j = jacobiSymbol a n'
prop_jacobiSymbol_2ndRule a (NonNegative n) = (jacobiSymbol a n') == (jacobiSymbol (a `rem` n') n')
    where
      n' = (n*2)+1
prop_jacobiSymbol_3rdRule1 a (NonNegative n) = (gcd a n' /= 1) ~> (jacobiSymbol a n' == 0)
    where
      n' = (n*2)+1
prop_jacobiSymbol_3rdRule2 a (NonNegative n) = (gcd a n' == 1) ~> (jacobiSymbol a n' /= 0)
    where
      n' = (n*2)+1
prop_jacobiSymbol_4thRule a b (NonNegative n) = jacobiSymbol (a*b) n' == (jacobiSymbol a n') * (jacobiSymbol b n')
    where
      n' = (n*2)+1
prop_jacobiSymbol_5thRule a (NonNegative m) (NonNegative n) = jacobiSymbol a (n'*m') == (jacobiSymbol a n') * (jacobiSymbol a m')
    where
      n' = (n*2)+1
      m' = (m*2)+1
prop_jacobiSymbol_6thRule (NonNegative m) (NonNegative n) = (gcd m' n' == 1 && m' `isNotDivisibleBy` 2 && n' `isNotDivisibleBy` 2) ~> ((jacobiSymbol n' m') * (jacobiSymbol m' n') == (-1) !^ (((m'-1) `quot` 2) * ((n'-1) `quot` 2)))
    where
      n' = (n*2)+1
      m' = (n*2)+1
prop_jacobiSymbol_n_n (Positive n) = jacobiSymbol n' n' == 0
    where
      n' = (n*2)+1
prop_jacobiSymbol_m_times_n _ (NonZero 1) = True
prop_jacobiSymbol_m_times_n _ (NonZero (-1)) = True
prop_jacobiSymbol_m_times_n (Positive n) (NonZero m) = jacobiSymbol (n'*m) n' == 0
    where
      n' = (n*2)+1
{-
prop_legendreSymbol_QuadRepoc_1 (Positive q) (Positive p) = (jacobiSymbol q' p') * (jacobiSymbol p' q') == (-1) !^ ((p'-1 `quot` 2) * (q'-1 `quot` 2))
    where
      q' = primes !! (q+1)
      p' = primes !! (p+1)

prop_legendreSymbol_QuadRepoc_2 (Positive a) (Positive p) = (jacobiSymbol a p') `mod` p' == a !^ (p'-1 `quot` 2)
    where
      p' = primes !! (p+1)

prop_legendreSymbol_Kronecker (Positive p) (Positive q) = signum (product ([[k/p' - i/q']| i <- [1..(q'-1 `quot` 2)], k <- [1..(p'-1 `quot` 2)]])) == jacobiSymbol p' q'
    where
      p' = primes !! (p+1)
      q' = primes !! (q+1)

prop_legendreSymbol_3p (Positive p) = l == (-1) !^ (floor ((p'*p'-1)/8))
    where
      p' = primes !! (p+2)
      l = jacobiSymbol 3 p'

prop_legendreSymbol_5p (Positive p) = l == (-1) !^ (floor ((2*p'+2)/5))
    where
      p' = primes !! (p+3)
      l = jacobiSymbol 5 p'
-}

prop_coerceMin_resultIsCoerced a mi = a' >= mi
    where
      a' = coerceMin a mi

prop_coerceMax_resultIsCoerced a ma = a' <= ma
    where
      a' = coerceMax a ma

prop_coerceIn_resultIsCoerced a mi ma = (mi <= ma) ~> (mi <= a' && a' <= ma)
    where
      a' = coerceIn a mi ma

prop_coerceIn_resultIsCoercedWithoutImplies a mi ma = mi' <= a' && a' <= ma'
    where
      mi' = min mi ma
      ma' = max mi ma
      a' = coerceIn a (min mi ma) (max mi ma)

prop_coerceInIntegral_resultIsCoerced a mi ma = (mi <= ma) ~> (mi <= a' && a' <= ma)
    where
      a' = coerceInIntegral a mi ma

prop_coerceInIntegral_resultIsCoercedWithoutImplies a mi ma = mi' <= a' && a' <= ma'
    where
      a' = coerceInIntegral a mi' ma'
      mi' = min mi ma
      ma' = max mi ma


--primes
prop_primes_correct (Positive n) = take n P.primes == take n primes
prop_primes_altcorrect (NonNegative n) = (P.primes !! n) == (primes !! n)

prop_trialDivision_prime_isPrime (NonNegative n) = trialDivision (primes !! n)

prop_fermat_prime_isPrime (NonNegative n) = fermat (primes !! n)

prop_wilson_prime_isPrime (NonNegative n) = wilson (primes !! n)

prop_solovay_strassen_prime_isPrime (NonNegative n) = solovayStrassen (primes !! n)

prop_miller_rabin_prime_isPrime (NonNegative n) = millerRabin (primes !! n)

return []

theCheck = do
  return []
  $forAllProperties (quickCheckWithResult stdArgs {maxSuccess = 1024})
  runTestTT jacobiTests


-- hunit

jacobiTests = TestList (jacobiSimpleTests ++ jacobiAdvTests)

jacobiSimpleTests = [jacobi_1_1, jacobi_1_3, jacobi_1_5, jacobi_1_7, jacobi_1_9, jacobi_1_11, jacobi_1_13, jacobi_2_1, jacobi_2_3, jacobi_2_5, jacobi_2_7, jacobi_2_9, jacobi_2_11, jacobi_2_13, jacobi_3_1, jacobi_3_3,  jacobi_3_5,  jacobi_3_7,  jacobi_3_9,  jacobi_3_11,  jacobi_3_13, jacobi_4_1,  jacobi_4_3,  jacobi_4_5,  jacobi_4_7,  jacobi_4_9,  jacobi_4_11,  jacobi_4_13, jacobi_5_1, jacobi_6_1,  jacobi_6_3,  jacobi_6_5,  jacobi_6_7,  jacobi_6_9,  jacobi_6_11,  jacobi_6_13, jacobi_7_1,  jacobi_7_3,  jacobi_7_5,  jacobi_7_7,  jacobi_7_9,  jacobi_7_11,  jacobi_7_13, jacobi_8_1,  jacobi_8_3,  jacobi_8_5,  jacobi_8_7,  jacobi_8_9,  jacobi_8_11,  jacobi_8_13, jacobi_9_1,  jacobi_9_3,  jacobi_9_5,  jacobi_9_7,  jacobi_9_9,  jacobi_9_11,  jacobi_9_13, jacobi_10_1,  jacobi_10_3,  jacobi_10_5,  jacobi_10_7,  jacobi_10_9,  jacobi_10_11,  jacobi_10_13, jacobi_11_1,  jacobi_11_3,  jacobi_11_5,  jacobi_11_7,  jacobi_11_9,  jacobi_11_11,  jacobi_11_13, jacobi_12_1,  jacobi_12_3,  jacobi_12_5,  jacobi_12_7,  jacobi_12_9,  jacobi_12_11,  jacobi_12_13, jacobi_13_1,  jacobi_13_3,  jacobi_13_5,  jacobi_13_7,  jacobi_13_9,  jacobi_13_11,  jacobi_13_13, jacobi_13_1]

jacobi_1_1   =  1 ~=? jacobiSymbol 1 1
jacobi_1_3   =  1 ~=? jacobiSymbol 1 3
jacobi_1_5   =  1 ~=? jacobiSymbol 1 5
jacobi_1_7   =  1 ~=? jacobiSymbol 1 7
jacobi_1_9   =  1 ~=? jacobiSymbol 1 9
jacobi_1_11  =  1 ~=? jacobiSymbol 1 11
jacobi_1_13  =  1 ~=? jacobiSymbol 1 13
jacobi_2_1   =  1 ~=? jacobiSymbol 2 1
jacobi_2_3   = -1 ~=? jacobiSymbol 2 3
jacobi_2_5   = -1 ~=? jacobiSymbol 2 5
jacobi_2_7   =  1 ~=? jacobiSymbol 2 7
jacobi_2_9   =  1 ~=? jacobiSymbol 2 9
jacobi_2_11  = -1 ~=? jacobiSymbol 2 11
jacobi_2_13  = -1 ~=? jacobiSymbol 2 13
jacobi_3_1   =  1 ~=? jacobiSymbol 3 1
jacobi_3_3   =  0 ~=? jacobiSymbol 3 3
jacobi_3_5   = -1 ~=? jacobiSymbol 3 5
jacobi_3_7   = -1 ~=? jacobiSymbol 3 7
jacobi_3_9   =  0 ~=? jacobiSymbol 3 9
jacobi_3_11  =  1 ~=? jacobiSymbol 3 11
jacobi_3_13  =  1 ~=? jacobiSymbol 3 13
jacobi_4_1   =  1 ~=? jacobiSymbol 4 1
jacobi_4_3   =  1 ~=? jacobiSymbol 4 3
jacobi_4_5   =  1 ~=? jacobiSymbol 4 5
jacobi_4_7   =  1 ~=? jacobiSymbol 4 7
jacobi_4_9   =  1 ~=? jacobiSymbol 4 9
jacobi_4_11  =  1 ~=? jacobiSymbol 4 11
jacobi_4_13  =  1 ~=? jacobiSymbol 4 13
jacobi_5_1   =  1 ~=? jacobiSymbol 5 1
jacobi_5_3   = -1 ~=? jacobiSymbol 5 3
jacobi_5_5   =  0 ~=? jacobiSymbol 5 5
jacobi_5_7   = -1 ~=? jacobiSymbol 5 7
jacobi_5_9   =  1 ~=? jacobiSymbol 5 9
jacobi_5_11  =  1 ~=? jacobiSymbol 5 11
jacobi_5_13  = -1 ~=? jacobiSymbol 5 13
jacobi_6_1   =  1 ~=? jacobiSymbol 6 1
jacobi_6_3   =  0 ~=? jacobiSymbol 6 3
jacobi_6_5   =  1 ~=? jacobiSymbol 6 5
jacobi_6_7   = -1 ~=? jacobiSymbol 6 7
jacobi_6_9   =  0 ~=? jacobiSymbol 6 9
jacobi_6_11  = -1 ~=? jacobiSymbol 6 11
jacobi_6_13  = -1 ~=? jacobiSymbol 6 13
jacobi_7_1   =  1 ~=? jacobiSymbol 7 1
jacobi_7_3   =  1 ~=? jacobiSymbol 7 3
jacobi_7_5   = -1 ~=? jacobiSymbol 7 5
jacobi_7_7   =  0 ~=? jacobiSymbol 7 7
jacobi_7_9   =  1 ~=? jacobiSymbol 7 9
jacobi_7_11  = -1 ~=? jacobiSymbol 7 11
jacobi_7_13  = -1 ~=? jacobiSymbol 7 13
jacobi_8_1   =  1 ~=? jacobiSymbol 8 1
jacobi_8_3   = -1 ~=? jacobiSymbol 8 3
jacobi_8_5   = -1 ~=? jacobiSymbol 8 5
jacobi_8_7   =  1 ~=? jacobiSymbol 8 7
jacobi_8_9   =  1 ~=? jacobiSymbol 8 9
jacobi_8_11  = -1 ~=? jacobiSymbol 8 11
jacobi_8_13  = -1 ~=? jacobiSymbol 8 13
jacobi_9_1   =  1 ~=? jacobiSymbol 9 1
jacobi_9_3   =  0 ~=? jacobiSymbol 9 3
jacobi_9_5   =  1 ~=? jacobiSymbol 9 5
jacobi_9_7   =  1 ~=? jacobiSymbol 9 7
jacobi_9_9   =  0 ~=? jacobiSymbol 9 9
jacobi_9_11  =  1 ~=? jacobiSymbol 9 11
jacobi_9_13  =  1 ~=? jacobiSymbol 9 13
jacobi_10_1  =  1 ~=? jacobiSymbol 10 1
jacobi_10_3  =  1 ~=? jacobiSymbol 10 3
jacobi_10_5  =  0 ~=? jacobiSymbol 10 5
jacobi_10_7  = -1 ~=? jacobiSymbol 10 7
jacobi_10_9  =  1 ~=? jacobiSymbol 10 9
jacobi_10_11 = -1 ~=? jacobiSymbol 10 11
jacobi_10_13 =  1 ~=? jacobiSymbol 10 13
jacobi_11_1  =  1 ~=? jacobiSymbol 11 1
jacobi_11_3  = -1 ~=? jacobiSymbol 11 3
jacobi_11_5  =  1 ~=? jacobiSymbol 11 5
jacobi_11_7  =  1 ~=? jacobiSymbol 11 7
jacobi_11_9  =  1 ~=? jacobiSymbol 11 9
jacobi_11_11 =  0 ~=? jacobiSymbol 11 11
jacobi_11_13 = -1 ~=? jacobiSymbol 11 13
jacobi_12_1  =  1 ~=? jacobiSymbol 12 1
jacobi_12_3  =  0 ~=? jacobiSymbol 12 3
jacobi_12_5  = -1 ~=? jacobiSymbol 12 5
jacobi_12_7  = -1 ~=? jacobiSymbol 12 7
jacobi_12_9  =  0 ~=? jacobiSymbol 12 9
jacobi_12_11 =  1 ~=? jacobiSymbol 12 11
jacobi_12_13 =  1 ~=? jacobiSymbol 12 13
jacobi_13_1  =  1 ~=? jacobiSymbol 13 1
jacobi_13_3  =  1 ~=? jacobiSymbol 13 3
jacobi_13_5  = -1 ~=? jacobiSymbol 13 5
jacobi_13_7  = -1 ~=? jacobiSymbol 13 7
jacobi_13_9  =  1 ~=? jacobiSymbol 13 9
jacobi_13_11 = -1 ~=? jacobiSymbol 13 11
jacobi_13_13 =  0 ~=? jacobiSymbol 13 13

jacobiAdvTests = [jacobi_1308779_1220923, jacobi_1866_1220923, jacobi_1904_1093, jacobi_P345_P2315, jacobi_P456548_P15633, jacobi_P1472_P806, jacobi_P136_P806, jacobi_P1512_P11]

jacobi_1308779_1220923 = 0 ~=? jacobiSymbol 1308779 1220923
jacobi_1866_1220923 = 1 ~=? jacobiSymbol 1866 1220923
jacobi_1904_1093 = -1 ~=? jacobiSymbol 1904 1093
jacobi_P345_P2315 = 1 ~=? jacobiSymbol (P.primes !! 345) (P.primes !! 2315)
jacobi_P456548_P15633 = 1 ~=? jacobiSymbol (P.primes !! 456548) (P.primes !! 15633)
jacobi_P1472_P806 = 1 ~=? jacobiSymbol (P.primes !! 1472) (P.primes !! 806)
jacobi_P136_P806 = -1 ~=? jacobiSymbol (P.primes !! 136) (P.primes !! 806)
jacobi_P1512_P11 = -1 ~=? jacobiSymbol (P.primes !! 1512) (P.primes !! 11)
