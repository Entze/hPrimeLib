module PrimeLib where
import Control.Parallel

sieve :: Integral a => [a] -> [a]
sieve [] = []
sieve (n:ns) = n : sieve(filter (`isNotDivisibleBy` n) ns)

(~>) :: Bool -> Bool -> Bool
(~>) True False = False
(~>) _ _ = True

(^!) :: Num a => a -> Int -> a
(^!) x n = x^n

(!^) :: Integral a => a -> Int -> a
(!^) _ 0 = 1
(!^) 0 _ = 0
(!^) a b = a * (a !^ (b-1))

coerceMin :: Ord a => a -> a -> a
coerceMin = max

coerceMax :: Ord a => a -> a -> a
coerceMax = min

coerceInIntegral v 0 0 = 0
coerceInIntegral v mi ma
    | mi == ma = mi
    | otherwise = (v `mod` (ma-mi)) + mi

coerceIn :: Ord a => a -> a -> a -> a
coerceIn v mi = coerceMax (coerceMin v mi)

intSqrt :: Integral a => a -> a
intSqrt 0 = 0
intSqrt 1 = 1
intSqrt n =
   let twopows = iterate (^!2) 2
       (lowerRoot, lowerN) =
          last $ takeWhile ((n>=) . snd) $ zip (1:twopows) twopows
       newtonStep x = div (x + div n x) 2
       iters = iterate newtonStep (intSqrt (div n lowerN) * lowerRoot)
       isRoot r  =  r^!2 <= n && n < (r+1)^!2
   in  head $ dropWhile (not . isRoot) iters

intLog :: Integral a => a -> a
intLog v = (floor . log . fromIntegral) v

--intLogBase :: Integral a => a -> Int -> a
intLogBase v b = floor ((log . fromIntegral) v / (log . fromIntegral) b)

factorOut v f
    | v `isDivisibleBy` f = 1 + (factorOut (v `quot` f) f)
    | otherwise = 0


powMod :: Integral a => a -> a -> a -> a
powMod _ _ 1 = 0
powMod _ 0 _ = 1
powMod b e m = powMod' (b `mod` m) e m 1
    where
      powMod' b e m r
          | e <= 0 = r
          | e `rem` 2 == 1 = powMod' b' e' m r'
          | otherwise = powMod' b' e' m r
          where
            b' = (b*b) `mod` m
            e' = e `div` 2
            r' = (r*b) `mod` m

powRem :: Integral a => a -> a -> a -> a
powRem _ _ 1 = 0
powRem _ 0 _ = 1
powRem b e m = powRem' (b `rem` m) e m 1
    where
      powRem' b e m r
          | e <= 0 = r
          | e `rem` 2 == 1 = powRem' b' e' m r'
          | otherwise = powRem' b' e' m r
          where
            b' = (b*b) `rem` m
            e' = e `quot` 2
            r' = (r*b) `rem` m


facMod :: Integral a => a -> a -> a
facMod 0 _ = 0
facMod _ 1 = 0
facMod _ (-1) = 0
facMod 1 m = 1 `mod` m
facMod 2 m = 2 `mod` m
facMod f m = (((f * (f-1)) `mod` m) * facMod (f-2) m) `mod` m

facRem :: Integral a => a -> a -> a
facRem 0 _ = 0
facRem _ 1 = 0
facRem _ (-1) = 0
facRem 1 m = 1 `rem` m
facRem 2 m = 2 `rem` m
facRem f m = (((f * (f-1)) `rem` m) * facRem (f-2) m) `rem` m

factorial 0 = 1
factorial 1 = 1
factorial 2 = 2
factorial 3 = 6
factorial 4 = 24
factorial 5 = 120
factorial 6 = 720
factorial 7 = 5040
factorial 8 = 40320
factorial 9 = 362880
factorial 10 = 3628800
factorial 20 = 2432902008176640000
factorial 30 = 265252859812191058636308480000000
factorial 40 = 815915283247897734345611269596115894272000000000
factorial 50 = 30414093201713378043612608166064768844377641568960512000000000000
factorial 60 = 8320987112741390144276341183223364380754172606361245952449277696409600000000000000
factorial 70 = 11978571669969891796072783721689098736458938142546425857555362864628009582789845319680000000000000000
factorial 80 = 71569457046263802294811533723186532165584657342365752577109445058227039255480148842668944867280814080000000000000000000
factorial 90 = 1485715964481761497309522733620825737885569961284688766942216863704985393094065876545992131370884059645617234469978112000000000000000000000
factorial 100 = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
factorial n = factorial (n-1) * n

fibonnaci :: Integral a => a -> a
fibonnaci 0 = 0
fibonnaci 1 = 1
fibonnaci n = fibonnaci (n-2) + fibonnaci (n-1)

isDivisibleBy :: Integral a => a -> a -> Bool
isDivisibleBy x y = x `rem` y == 0

isNotDivisibleBy :: Integral a => a -> a -> Bool
isNotDivisibleBy x y = x `rem` y /= 0

jacobiSymbol 1 _ = 1
jacobiSymbol _ 1 = 1
jacobiSymbol 0 _ = 0
jacobiSymbol _ 0 = 0
jacobiSymbol (-1) n
    | n `rem` 4 == 3 = (-1)
    | otherwise = 1
jacobiSymbol 4 n
    | gcd 4 n /= 1 = 0
    | otherwise = 1
jacobiSymbol 2 n
    | n8 == 1 || n8 == 7 = 1
    | n8 == 3 || n8 == 5 = -1
    | otherwise = 0
    where
      n8 = n `rem` 8
jacobiSymbol a n
    | gcd a n /= 1 = 0
    | a < 0 = jacobiSymbol (-1) n * jacobiSymbol (a*(-1)) n
    | otherwise = jacobiSymbol' (a `rem` n) n
    where
      jacobiSymbol' 1 _ = 1
      jacobiSymbol' _ 1 = 1
      jacobiSymbol' 0 _ = 0
      jacobiSymbol' _ 0 = 0
      jacobiSymbol' 2 n
          | n8 == 1 || n8 == 7 = 1
          | n8 == 3 || n8 == 5 = -1
          | otherwise = 0
          where
            n8 = n `rem` 8
      jacobiSymbol' 4 n
          | gcd 4 n /= 1 = 0
          | otherwise = 1
      jacobiSymbol' a n
          | gcd a n /= 1 = 0
          | a `isDivisibleBy` 2 = (jacobiSymbol' 2 n) * (jacobiSymbol' (a `quot` 2) n)
          | otherwise = (quadReproc (a `rem` 4) (n `rem` 4)) * jacobiSymbol' (n `rem` a) a
          where
            quadReproc 1 _ = 1
            quadReproc _ 1 = 1
            quadReproc 3 3 = -1
            quadReproc _ _ = 0

anyPar2 :: (a -> Bool) -> [a] -> Bool
anyPar2 _ []               = False
anyPar2 f [l1]             = f l1
anyPar2 f [l1, l2]         = f l2 `par` f l1 || f l2
anyPar2 f (l1:l2:l)        = f l2 `par` f l1 || f l2 || anyPar2 f l

allPar2 :: (a -> Bool) -> [a] -> Bool
allPar2 _ []               = True
allPar2 f [l1]             = f l1
allPar2 f [l1, l2]         = f l2 `par` f l1 && f l2
allPar2 f (l1:l2:l)        = f l2 `par` f l1 && f l2 && allPar2 f l

anyPar4 :: (a -> Bool) -> [a] -> Bool
anyPar4 _ []               = False
anyPar4 f [l1]             =                                  f l1
anyPar4 f [l1, l2]         =                       f l2 `par` f l1 || f l2
anyPar4 f [l1, l2, l3]     =            f l3 `par` f l2 `par` f l1 || f l2 || f l3
anyPar4 f [l1, l2, l3, l4] = f l4 `par` f l3 `par` f l2 `par` f l1 || f l2 || f l3 || f l4
anyPar4 f (l1:l2:l3:l4:l)  = f l4 `par` f l3 `par` f l2 `par` f l1 || f l2 || f l3 || f l4 || anyPar4 f l

allPar4 :: (a -> Bool) -> [a] -> Bool
allPar4 _ []               = True
allPar4 f [l1]             =                                  f l1
allPar4 f [l1, l2]         =                       f l2 `par` f l1 && f l2
allPar4 f [l1, l2, l3]     =            f l3 `par` f l2 `par` f l1 && f l2 && f l3
allPar4 f [l1, l2, l3, l4] = f l4 `par` f l3 `par` f l2 `par` f l1 && f l2 && f l3 && f l4
allPar4 f (l1:l2:l3:l4:l)  = f l4 `par` f l3 `par` f l2 `par` f l1 && f l2 && f l3 && f l4 && allPar4 f l
