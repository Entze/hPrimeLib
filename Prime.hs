module Prime where

import PrimeLib

-- prime generation

initPrimes :: Integral a => [a]
initPrimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]

primes :: Integral a => [a]
primes = sieveOfEratosthenes initPrimes

sieveOfEratosthenes :: Integral a => [a] -> [a]
sieveOfEratosthenes [] = sieveOfEratosthenes [2,3]
sieveOfEratosthenes [2] = sieveOfEratosthenes [2,3]
sieveOfEratosthenes ps = sieve (ps++candidates)
    where
      candidates = [6*n+r | n <-[(last ps `quot` 6)+1..], r<-[-1,1]]

-- deterministic tests

trialDivision :: Integral a => a -> Bool
trialDivision p = not (any (isDivisibleBy p) (takeWhile (<= intSqrt p) primes))

wilson :: Integral a => a -> Bool
wilson 0 = False
wilson 1 = False
wilson p = facRem (p-1) p == p-1

-- probalistic tests

fermat :: Integral a => a -> a -> Bool
fermat 0 _ = False
fermat 1 _ = False
fermat 2 _ = True
fermat 3 _ = True
fermat 4 _ = False
fermat 5 _ = True
fermat p a = fermat' p (coerceInIntegral a 2 (p-2))
    where
      fermat' p a = powRem a (p-1) p == 1

solovayStrassen :: Integral a => a -> a -> Bool
solovayStrassen 0 _ = False
solovayStrassen 1 _ = False
solovayStrassen 2 _ = True
solovayStrassen 3 _ = True
solovayStrassen 4 _ = False
solovayStrassen 5 _ = True
solovayStrassen p a = p `isNotDivisibleBy` 2 && x /= 0 && (c == x || c == (p+x))
    where
      c = powRem a' n p
      a' = coerceInIntegral a 2 (p-1)
      x = jacobiSymbol a' p
      n = (p-1) `quot` 2

millerRabin :: Integral a => a -> a -> Bool
millerRabin 0 _ = False
millerRabin 1 _ = False
millerRabin 2 _ = True
millerRabin 3 _ = True
millerRabin 4 _ = False
millerRabin 5 _ = True
millerRabin 6 _ = False
millerRabin 7 _ = True
millerRabin p a = p `isNotDivisibleBy` 2 && ((x == 1 || x == p-1) || innerTest (r-1) (x*x `rem` p))
    where
      innerTest 0 _ = False
      innerTest 1 _ = False
      innerTest _ 1 = False
      innerTest t v
          | v == p-1 = True
          | otherwise = innerTest (t-1) (v*v `rem` p)
      x = powRem a' d p
      a' = coerceInIntegral a 2 (p-2)
      (r,d) = factorOut (p-1) 2

-- heuristic tests

{- does not really work
 selfridge :: Integral a => a -> Bool
selfridge p = p `isNotDivisibleBy` 2 && abs (p `rem` 5) == 2 && fermat p 2 && fibonnaci (p+1) `isDivisibleBy` p
-}
